package com.pact.example.pact.demo;

//import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;

import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import com.pact.example.pact.demo.config.ConsumerConfig;
import com.pact.example.pact.demo.domain.Customer;
import com.pact.example.pact.demo.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ComponentScan(basePackages = "com.pact.example.pact.demo")
@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "test_provider_demo")

class PactClientDemoApplicationTest {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ConsumerConfig config() {
		return new ConsumerConfig();
	}

	@Autowired
	CustomerService customerService;

	@Autowired
	ConsumerConfig consumerConfig;


	@Test
	public void runTest(MockServer mockServer) throws IOException {

		Customer expectedResponse = new Customer();
		expectedResponse.setAge(3451);
		expectedResponse.setName("Muster");
		expectedResponse.setSurename("Hans");


		String url = mockServer.getUrl();

		consumerConfig.setUrl(url);
		consumerConfig.setPort(mockServer.getPort());

		HttpHeaders headers =  new HttpHeaders();
		HttpEntity request = new HttpEntity<>(headers);
		Customer response = customerService.getCustomer();

		assertEquals(response, expectedResponse);
	}


	@Pact(provider = "test_provider_demo", consumer="test_consumer_demo")
	public RequestResponsePact createPact(PactDslWithProvider builder){
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");

		return builder
				.given("test state")
				.uponReceiving("ExampleJavaConsumerPactRuleTest test interaction")
					.path("/customer")
					.method("GET")
				.willRespondWith()
					.status(200)
					.headers(headers)
					.body("{\"age\": 3451, \"name\": \"Muster\", \"surename\": \"Hans\"}")
				.toPact();
	}
}
