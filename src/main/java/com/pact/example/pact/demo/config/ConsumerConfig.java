package com.pact.example.pact.demo.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

@Data
@org.springframework.context.annotation.Configuration
public class ConsumerConfig {

    private String url;
    private int port;
}
