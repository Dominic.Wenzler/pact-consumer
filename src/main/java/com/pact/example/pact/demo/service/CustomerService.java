package com.pact.example.pact.demo.service;

import com.pact.example.pact.demo.config.ConsumerConfig;
import com.pact.example.pact.demo.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ConsumerConfig consumerConfig;


    public Customer getCustomer(){
        HttpHeaders headers =  new HttpHeaders();
        HttpEntity request = new HttpEntity<>(headers);
        ResponseEntity<Customer> resp = restTemplate.exchange(consumerConfig.getUrl()+ "/customer", HttpMethod.GET, request, Customer.class);

        return resp.getBody();
    }
}
