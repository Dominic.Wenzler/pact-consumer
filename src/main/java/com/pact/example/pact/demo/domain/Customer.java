package com.pact.example.pact.demo.domain;

import lombok.Data;

@Data
public class Customer {

    private int age;
    private String name;
    private String surename;


}
