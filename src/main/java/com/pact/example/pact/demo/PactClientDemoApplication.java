package com.pact.example.pact.demo;

import com.pact.example.pact.demo.config.ConsumerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class PactClientDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PactClientDemoApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ConsumerConfig config() {
		return new ConsumerConfig();
	}
}
